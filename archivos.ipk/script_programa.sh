#!/bin/sh
# BOOT0 GPIO39
# BOOT1 GPIO19
# TRST GPIO37
# BOOT0 = 0 Main flash
# BOOT0 = 1 Embedded SRAM
#omega2-ctrl gpiomux set pwm0 gpio
#omega2-ctrl gpiomux set pwm1 gpio
# Boot1 = 1
echo "19" >  "/sys/class/gpio/unexport"
echo "19" >  "/sys/class/gpio/export"
echo "out" > "/sys/class/gpio/gpio19/direction"
echo "1" >  "/sys/class/gpio/gpio19/value"
# Boot0 = 1
echo "39" >  "/sys/class/gpio/unexport"
echo "39" >  "/sys/class/gpio/export"
echo "out" > "/sys/class/gpio/gpio39/direction"
echo "1" >   "/sys/class/gpio/gpio39/value"
sleep 1
# Reset
echo "37" >  "/sys/class/gpio/unexport"
echo "37" >  "/sys/class/gpio/export"
echo "out" > "/sys/class/gpio/gpio37/direction"
echo "1" >   "/sys/class/gpio/gpio37/value"
sleep 1
echo "0" >  "/sys/class/gpio/gpio37/value"
sleep 1
echo "1" >   "/sys/class/gpio/gpio37/value"
openocd -f onion.cfg

echo "0" >   "/sys/class/gpio/gpio39/value"

# Reset
#echo "15" >  "/sys/class/gpio/unexport"
#echo "15" >  "/sys/class/gpio/export"
#echo "out" > "/sys/class/gpio/gpio15/direction"
echo "1" >   "/sys/class/gpio/gpio37/value"
sleep 1
echo "0" >  "/sys/class/gpio/gpio37/value"
sleep 1
echo "1" >   "/sys/class/gpio/gpio37/value"
