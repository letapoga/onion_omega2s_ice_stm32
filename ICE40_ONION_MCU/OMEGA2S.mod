PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
XCVR_OMEGA2S
$EndINDEX
$MODULE XCVR_OMEGA2S
Po 0 0 0 15 00000000 00000000 ~~
Li XCVR_OMEGA2S
Cd 
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 -8.48881 -17.996 1.0034 1.0034 0 0.05 N V 21 "XCVR_OMEGA2S"
T1 -7.83943 18.6997 1.00185 1.00185 0 0.05 N V 21 "VAL**"
DS -10 17 10 17 0.127 24
DS 10 17 10 -17 0.127 24
DS -10 -17 -10 17 0.127 24
DC -10.75 -16 -10.65 -16 0.2 21
DS -10 15.95 -10 17 0.127 21
DS -10 17 -8.35 17 0.127 21
DS -10 -15.95 -10 -17 0.127 21
DS -10 -17 10 -17 0.127 21
DS 10 -17 10 -15.95 0.127 21
DS 8.35 17 10 17 0.127 21
DS 10 17 10 15.95 0.127 21
DS -11 -17.25 -11 18 0.05 26
DS -11 18 11 18 0.05 26
DS 11 18 11 -17.25 0.05 26
DS 11 -17.25 -11 -17.25 0.05 26
DS -10 -17 10 -17 0.127 24
DC -10.75 -16 -10.65 -16 0.2 24
$PAD
Sh "25" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -10 15.24
$EndPAD
$PAD
Sh "24" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -10 13.97
$EndPAD
$PAD
Sh "23" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -10 12.7
$EndPAD
$PAD
Sh "22" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -10 11.43
$EndPAD
$PAD
Sh "21" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -10 10.16
$EndPAD
$PAD
Sh "20" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -10 8.89
$EndPAD
$PAD
Sh "19" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -10 7.62
$EndPAD
$PAD
Sh "18" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -10 6.35
$EndPAD
$PAD
Sh "17" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -10 5.08
$EndPAD
$PAD
Sh "16" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -10 3.81
$EndPAD
$PAD
Sh "15" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -10 2.54
$EndPAD
$PAD
Sh "12" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -10 -1.27
$EndPAD
$PAD
Sh "13" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -10 0
$EndPAD
$PAD
Sh "14" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -10 1.27
$EndPAD
$PAD
Sh "11" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -10 -2.54
$EndPAD
$PAD
Sh "10" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -10 -3.81
$EndPAD
$PAD
Sh "9" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -10 -5.08
$EndPAD
$PAD
Sh "8" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -10 -6.35
$EndPAD
$PAD
Sh "7" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -10 -7.62
$EndPAD
$PAD
Sh "6" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -10 -8.89
$EndPAD
$PAD
Sh "5" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -10 -10.16
$EndPAD
$PAD
Sh "4" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -10 -11.43
$EndPAD
$PAD
Sh "3" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -10 -12.7
$EndPAD
$PAD
Sh "2" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -10 -13.97
$EndPAD
$PAD
Sh "1" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -10 -15.24
$EndPAD
$PAD
Sh "26" R 0.9 1.5 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -7.62 17
$EndPAD
$PAD
Sh "27" R 0.9 1.5 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -6.35 17
$EndPAD
$PAD
Sh "28" R 0.9 1.5 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -5.08 17
$EndPAD
$PAD
Sh "29" R 0.9 1.5 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -3.81 17
$EndPAD
$PAD
Sh "30" R 0.9 1.5 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.54 17
$EndPAD
$PAD
Sh "31" R 0.9 1.5 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -1.27 17
$EndPAD
$PAD
Sh "32" R 0.9 1.5 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 0 17
$EndPAD
$PAD
Sh "33" R 0.9 1.5 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 1.27 17
$EndPAD
$PAD
Sh "34" R 0.9 1.5 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.54 17
$EndPAD
$PAD
Sh "35" R 0.9 1.5 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 3.81 17
$EndPAD
$PAD
Sh "36" R 0.9 1.5 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 5.08 17
$EndPAD
$PAD
Sh "37" R 0.9 1.5 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 6.35 17
$EndPAD
$PAD
Sh "38" R 0.9 1.5 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 7.62 17
$EndPAD
$PAD
Sh "39" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 10 15.24
$EndPAD
$PAD
Sh "40" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 10 13.97
$EndPAD
$PAD
Sh "41" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 10 12.7
$EndPAD
$PAD
Sh "42" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 10 11.43
$EndPAD
$PAD
Sh "43" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 10 10.16
$EndPAD
$PAD
Sh "44" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 10 8.89
$EndPAD
$PAD
Sh "45" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 10 7.62
$EndPAD
$PAD
Sh "46" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 10 6.35
$EndPAD
$PAD
Sh "47" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 10 5.08
$EndPAD
$PAD
Sh "48" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 10 3.81
$EndPAD
$PAD
Sh "49" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 10 2.54
$EndPAD
$PAD
Sh "50" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 10 1.27
$EndPAD
$PAD
Sh "51" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 10 0
$EndPAD
$PAD
Sh "52" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 10 -1.27
$EndPAD
$PAD
Sh "53" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 10 -2.54
$EndPAD
$PAD
Sh "54" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 10 -3.81
$EndPAD
$PAD
Sh "55" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 10 -5.08
$EndPAD
$PAD
Sh "56" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 10 -6.35
$EndPAD
$PAD
Sh "57" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 10 -7.62
$EndPAD
$PAD
Sh "58" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 10 -8.89
$EndPAD
$PAD
Sh "59" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 10 -10.16
$EndPAD
$PAD
Sh "60" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 10 -11.43
$EndPAD
$PAD
Sh "61" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 10 -12.7
$EndPAD
$PAD
Sh "62" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 10 -13.97
$EndPAD
$PAD
Sh "63" R 0.9 1.5 0 0 900
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 10 -15.24
$EndPAD
$EndMODULE XCVR_OMEGA2S
