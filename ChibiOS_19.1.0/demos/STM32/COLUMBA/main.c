/*
    ChibiOS - Copyright (C) 2006..2018 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include "ch.h"
#include "hal.h"


static void ledoff(void *p) {
  (void)p;
  palClearPad(GPIOB, GPIOB_LED4);
}

/*
 * This callback is invoked when a transmission buffer has been completely
 * read by the driver.




/*
 * This is a periodic thread that does absolutely nothing except flashing
 * LEDs.
 */
static THD_WORKING_AREA(waThread1, 128);
static THD_FUNCTION(Thread1, arg) {
  palSetPadMode(GPIOD, 2, PAL_MODE_OUTPUT_PUSHPULL);
  (void)arg;
  chRegSetThreadName("blinker");
  while (true) {
    palSetPad(GPIOD, 2);
    chThdSleepMilliseconds(200);
    palClearPad(GPIOD, 2);
    chThdSleepMilliseconds(100);
  }
}

/*
 * Application entry point.
 */
int main(void) {

  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  halInit();
  chSysInit();

  /*
   * Starts the LED blinker thread.
   */
  //chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO + 1, Thread1, NULL);

  /*
   * Normal main() thread activity
   */
   palSetPadMode(GPIOD, 2, PAL_MODE_OUTPUT_PUSHPULL);
//   chprintf((BaseSequentialStream *) &SD4,"Reading started \r\n ");
   while (true) {
    palSetPad(GPIOD, 2);
    chThdSleepMilliseconds(100);
    palClearPad(GPIOD, 2);
  //  chprintf((BaseSequentialStream *) &SD4,"Blink \r\n ");
    chThdSleepMilliseconds(100);
  }

  
}
